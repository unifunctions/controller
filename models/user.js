const mongoose = require('mongoose');

const User = mongoose.model('User', new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    index: true,
  },
  email: {
    type: String,
    unique: true,
    required: true,
    index: true,
  },
  salt: {
    type: String,
    required: true,
  },
  hash: {
    type: String,
    require: true,
  }
}));

/**
 * Create a User in MongoDB and return a Promise.
 * @param {Object} info Information of the User to be created.
 */
const create = (info) => {
  return User.create({
    ...info,
  });
}

/**
 * Find a User with certain conditions and return a Promise.
 * @param {Object} conditions Conditions for finding a User.
 */
const read = (conditions) => {
  return User.findOne(conditions).exec();
}

/**
 * Find a User with its username and return a Promise.
 * @param {String} username The username of the user to be found.
 */
const readByUsername = (username) => {
  return User.findOne({ username: username }).exec();
}

/**
 * Find a user with its email and return a Promise.
 * @param {String} email Email of the user to be found.
 */
const readByEmail = (email) => {
  return User.findOne({ email: email }).exec();
}

/**
 * Find a User in MongoDB with its _id and return a Promise.
 * @param {String} id _id of the User to be found.
 */
const readById = (id) => {
  return User.findById(id).exec();
}

/**
 * Find a User with certain conditions and update its info, and return a Promise.
 * @param {Object} conditions Conditions for finding a User.
 * @param {Object} updates Updates to be made to the User.
 */
const update = (conditions, updates) => {
  return User.findOneAndUpdate(conditions, updates, { new: true }).exec();
}

/**
 * Find a User with certain conditions, delete the User from MongoDB and return a Promise.
 * @param {Object} conditions Conditions for finding a User.
 */
const deleteByConditions = (conditions) => {
  return User.findOneAndDelete(conditions).exec();
}

/**
 * Find a User by its _id, delete it from MongoDB and return a Promise.
 * @param {String} id _id of the User to be deleted.
 */
const deleteById = (id) => {
  return User.findByIdAndDelete(id).exec();
}

module.exports = { create, read, readByUsername, readByEmail, readById, update, deleteByConditions, deleteById };
