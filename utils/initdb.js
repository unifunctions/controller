const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')
const userModel = require('../models/user');

const mongodb = 'mongodb://localhost:27017/unifunctions';
const user = {
  username: 'unifuncRoot',
  password: 'unifunctions92644264',
  email: 'qriousgabriel@gmail.com',
}
const saltRound = 10;

mongoose.connect(mongodb);
const db = mongoose.connection;
db.on('connected', () => { console.log('MongoDB connected.') });
db.on('error', () => {
  console.warn('Error connecting to MongoDB');
});

const f = async () => {
  try {
    console.log(`Creating user '${user.username}'...`);
    const salt = await bcrypt.genSalt(saltRound);
    console.log(`Salt: ${salt}`);
    const hash = await bcrypt.hash(user.password, salt);
    console.log(`Hash: ${hash}`);
    user.salt = salt;
    user.hash = hash;
    const newUser = await userModel.create(user);
    console.log(newUser);
  } catch (err) {
    console.warn(err);
  }
}

f();

process.exit();
