const userModel = require('../models/user');

const validateUsername = (username) => {
  if (username.length < 8 || username.length >= 30) {
    return false;
  }
  const regExp = /^[a-zA-Z0-9_\-\.]+$/;
  if (!regExp.exec(username)) {
    return false;
  }
  return true;
}

const validateEmail = (email) => {
  const regExp = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  if (!regExp.exec(email)) {
    return false;
  }
  return true;
}

// Create a new user.
const create = async (req, res) => {
  try {
    const username = req.body.username;
    const email = req.body.email;
    const hash = req.body.hash;
    if (!validateUsername(username)) {
      res.status(422).json({ errMessage: 'Invalid username.' });
    } else if (!validateEmail(email)) {
      res.status(422).json({ errMessage: 'Invalid email address.' })
    } else if (await userModel.readByUsername(username)) {
      res.status(409).json({ errMessage: `Username ${username} already taken.` });
    } else if (await userModel.readByEmail(email)) {
      res.status(409).json({ errMessage: `Email ${email} already taken.` });
    } else {
      await userModel.create({ username, email, hash });
      res.status(201).json({ message: `User ${username} created.` });
    }
  } catch (err) {
    res.status(500).json({ errMessage: 'Internal Server Error.' });
  }
}

// Authenticate a user.
const authenticate = async (req, res) => {
  try {
    const usernameOrEmail = req.body.usernameOrEmail;
    const hash = req.body.hash;
    const user = usernameOrEmail == 'username' ? await userModel.readByUsername(req.body.username) : await userModel.readByEmail(req.body.email);
    if (hash == user.hash) {
      res.status(200).json({ username, email } = user);
    } else {
      res.status(401).json({ errMessage: 'Incorrect credentials.' });
    }
  } catch (err) {
    res.status(500).json({ errMessage: 'Internal Server Error.' });
  }
}

// Update username
const updateUsername = async (req, res) => {
  try {
    const updates = req.body.updates;
    // check session 
    if (!validateUsername(updates.username)) {
      res.status(422).json({ errMessage: 'Invalid username.' });
    } else if (!validateEmail(updates.email)) {
      res.status(422).json({ errMessage: 'Invalid email address.' });
    }
    // } else if () {

    // }
  } catch (err) {
    res.status(500).json({ errMessage: 'Internal Server Error.' });
  }
}
